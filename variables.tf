variable "rgname" {
  type = string
}

variable "rglocation" {
  type = string
}

variable "storage_account_name" {
  type = string
}

variable "container_name" {
  type = string
}
