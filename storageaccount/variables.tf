variable "rgname" {
  type = string
}

variable "rglocation" {
  type = string
}

variable "storageaccount_name" {
  type = string
  default = "charviarjun"
}

variable "account_tier" {
  type = string
  default = "Standard"
}

variable "account_replication_type" {
  type = string
  default = "LRS"
}

variable "container_name" {
  type = string
}

variable "container_access_type" {
  type = string
  default = "private"
}
