module "rg" {
  source = "./resourcegroup"
  rgname = var.rgname
  rglocation = var.rglocation
}

module "storageaccount" {
  source = "./storageaccount"
  rgname = module.rg.rgname
  rglocation = module.rg.rglocation
  storageaccount_name = var.storage_account_name
  container_name = var.container_name
}
